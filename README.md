# Cpp lessons

Simple test framework for unit testing cpp code.

# Build

To config desired version use CMake command

```
cmake -DUSE_VERSION=your_version . -B ./build
```

To see how common assert is disabled config with flag `CMAKE_BUILD_TYPE=Release`

```
cmake -DUSE_VERSION=2 -DCMAKE_BUILD_TYPE=Release . -B ./build
```

To build configured version use

```
cd build
make
```

To run builded version 
```
./test_framework_your_version
```