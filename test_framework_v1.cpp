#include <iostream>
#include <cassert>

int good_sum(int x, int y) {
    return x + y;
}

int bad_sum(int x, int y) {
    return x + y - 1;
}

void TestSum1() {
    assert(good_sum( 2,  3) ==  5);
    assert(good_sum(-2, -3) == -5);
    assert(good_sum(-2,  2) ==  0);
    std::cout << "TestSum OK" << std::endl;
}

void TestSum2() {
    assert(bad_sum( 2,  3) ==  5);
    assert(bad_sum(-2, -3) == -5);
    assert(bad_sum(-2,  2) ==  0);
    std::cout << "TestSum OK" << std::endl;
}
int main() {
    TestSum1();
    TestSum2();
    return 0;
}