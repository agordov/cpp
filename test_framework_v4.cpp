#include <iostream>
#include <string>
#include <map>
#include <set>
#include <cassert>
#include <exception>
#include <sstream>
/*
 *
 *  Two words are synonyms if there is a mapped word for them
 *
 */

using Synonyms = std::map<std::string, std::set<std::string>>;

// сократили запись типа и везде изменили на Synonyms
void AddSynonyms(Synonyms &synonyms, const std::string &first_word, const std::string &second_word)
{
    synonyms[second_word].insert(first_word);
    synonyms[first_word].insert(first_word); // тут должен не сработать AddSynonyms
}

size_t GetSynonymCount(Synonyms &synonyms, const std::string &word)
{
    return synonyms[word].size();
}

bool AreSynonyms(Synonyms &synonyms, const std::string &first_word, const std::string &second_word)
{
    return synonyms[first_word].count(second_word) == 1;
}

void TestAddSynonyms()
{ // тестируем AddSynonyms
    {
        Synonyms empty; // тест 1
        AddSynonyms(empty, "a", "b");
        const Synonyms expected = {
            {"a", {"b"}}, // ожидаем, что при добавлении синонимов появятся две записи в
            // словаре
            {"b", {"a"}}};
        assert(empty == expected);
    }
    {
        // заметим, что мы формируем корректный словарь и ожидаем, что он останется корректным
        Synonyms synonyms = {              // если вдруг корректность нарушится, то assert скажет, где
                             {"a", {"b"}}, // тест 2
                             {"b", {"a", "c"}},
                             {"c", {"b"}}};
        AddSynonyms(synonyms, "a", "c");
        const Synonyms expected = {
            {"a", {"b", "c"}},
            {"b", {"a", "c"}},
            {"c", {"a", "b"}}};
        assert(synonyms == expected);
    }
    std::cout << " TestAddSynonyms OK" << std::endl;
}

template <class T> // учимся выводить в поток set
std::ostream &operator<<(std::ostream &os, const std::set<T> &s)
{
    os << "{";
    bool first = true;
    for (const auto &x : s)
    {
        if (!first)
        {
            os << ", ";
        }
        first = false;
        os << x;
    }
    return os << "}";
}

template <class K, class V> // учимся выводить в поток map
std::ostream &operator<<(std::ostream &os, const std::map<K, V> &m)
{
    os << "{";
    bool first = true; // грамотная расстановка запятых
    for (const auto &kv : m)
    {
        if (!first)
        {
            os << ", ";
        }
        first = false;
        os << kv.first << ": " << kv.second;
    }
    return os << "}";
}



template <class T, class U>
void AssertEqual(class T &t, class U &u, const std::string &hint)
{
    if (t != u)
    {
        std::ostringstream os;
        os << "Assertion failed: " << t << "!=" << u << " Hint: " << hint << std::endl;
        throw std::runtime_error(os);
    }
}

void TestCount()
{ // тестируем Count
    {
        Synonyms empty;
        AssertEqual(GetSynonymCount(empty, "a"), 0u, " Synomym count for empty dict a");
        AssertEqual(GetSynonymCount(empty, "b"), 0u, " Synomym count for empty dict b");
    }
    {
        Synonyms synonyms = {
            {"a", {"b", "c"}},
            {"b", {"a"}},
            {"c", {"a"}}};
        AssertEqual(GetSynonymCount(synonyms, "a"), 2u, " Nonempty dict , count a");
        AssertEqual(GetSynonymCount(synonyms, "b"), 1u, " Nonempty dict , count b");
        AssertEqual(GetSynonymCount(synonyms, "z"), 0u, " Nonempty dict , count z");
    }
    std::cout << " TestCount OK" << std::endl;
}

void TestAreSynonyms()
{ // тестируем AreSynonyms
    {
        Synonyms empty; // пустой словарь для любых двух слов вернёт false
        AssertEqual(AreSynonyms(empty, "a", "b"), false);
        AssertEqual(AreSynonyms(empty, "b", "a"), false);
    }
    {
        Synonyms synonyms = {
            {"a", {"b", "c"}},
            {"b", {"a"}},
            {"c", {"a"}}};
        AssertEqual(AreSynonyms(synonyms, "a", "b"), true);
        AssertEqual(AreSynonyms(synonyms, "b", "a"), true);
        AssertEqual(AreSynonyms(synonyms, "a", "c"), true);
        AssertEqual(AreSynonyms(synonyms, "c", "a"), true);
        AssertEqual(!AreSynonyms(synonyms, "b", "c"), true);
        AssertEqual(!AreSynonyms(synonyms, "c", "b"), true);
    }
    std::cout << " TestAreSynonyms OK" << std::endl;
}

template <class TestFunc>
void RunTest(TestFunc func, const std::string &test_name)
{ // передаём тест и его имя
    try
    {
        func();
    }
    catch (runtime_error &e)
    {
        cerr << test_name << " fail: " << e.what() << endl; // ловим исключение
    }
}

void TestAll()
{ // функция, вызывающая все тесты
    TestCount();
    TestAreSynonyms();
    TestAddSynonyms();
}
int main()
{

    RunTest(TestAreSynonyms, " TestAreSynonyms ");
    RunTest(TestCount, " TestCount ");
    RunTest(TestAddSynonyms, " TestAddSynonyms ");
}