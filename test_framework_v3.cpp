#include <iostream>
#include <string>
#include <map>
#include <set>
#include <cassert>
#include <exception>
#include <sstream>
/*
 *
 *  Two words are synonyms if there is a mapped word for them
 *
 */

using Synonyms = std::map<std::string, std::set<std::string>>;

// сократили запись типа и везде изменили на Synonyms
void AddSynonyms(Synonyms &synonyms, const std::string &first_word, const std::string &second_word)
{
    synonyms[second_word].insert(first_word);
    synonyms[first_word].insert(first_word); // тут должен не сработать AddSynonyms
}

size_t GetSynonymCount(Synonyms &synonyms, const std::string &word)
{
    return synonyms[word].size();
}

bool AreSynonyms(Synonyms &synonyms, const std::string &first_word, const std::string &second_word)
{
    return synonyms[first_word].count(second_word) == 1;
}

void TestAddSynonyms()
{ // тестируем AddSynonyms
    {
        Synonyms empty; // тест 1
        AddSynonyms(empty, "a", "b");
        const Synonyms expected = {
            {"a", {"b"}}, // ожидаем, что при добавлении синонимов появятся две записи в
            // словаре
            {"b", {"a"}}};
        assert(empty == expected);
    }
    {
        // заметим, что мы формируем корректный словарь и ожидаем, что он останется корректным
        Synonyms synonyms = {              // если вдруг корректность нарушится, то assert скажет, где
                             {"a", {"b"}}, // тест 2
                             {"b", {"a", "c"}},
                             {"c", {"b"}}};
        AddSynonyms(synonyms, "a", "c");
        const Synonyms expected = {
            {"a", {"b", "c"}},
            {"b", {"a", "c"}},
            {"c", {"a", "b"}}};
        assert(synonyms == expected);
    }
    std::cout << " TestAddSynonyms OK" << std::endl;
}

template <class T> // учимся выводить в поток set
std::ostream &operator<<(std::ostream &os, const std::set<T> &s)
{
    os << "{";
    bool first = true;
    for (const auto &x : s)
    {
        if (!first)
        {
            os << ", ";
        }
        first = false;
        os << x;
    }
    return os << "}";
}

template <class K, class V> // учимся выводить в поток map
std::ostream &operator<<(std::ostream &os, const std::map<K, V> &m)
{
    os << "{";
    bool first = true; // грамотная расстановка запятых
    for (const auto &kv : m)
    {
        if (!first)
        {
            os << ", ";
        }
        first = false;
        os << kv.first << ": " << kv.second;
    }
    return os << "}";
}

template <class T, class U>
void AssertEqual(class T &t, class U &u, const std::string &hint)
{
    if (t != u)
    {
        std::ostringstream os;
        os << "Assertion failed: " << t << "!=" << u << " Hint: " << hint << "\n";
        throw std::runtime_error(os);
    }
}

void TestCount()
{ // тестируем Count
    {
        Synonyms empty;
        AssertEqual(GetSynonymCount(empty, "a"), 0u, " Synomym count for empty dict a");
        AssertEqual(GetSynonymCount(empty, "b"), 0u, " Synomym count for empty dict b");
    }
    {
        Synonyms synonyms = {
            {"a", {"b", "c"}},
            {"b", {"a"}},
            {"c", {"a"}}};
        AssertEqual(GetSynonymCount(synonyms, "a"), 2u, " Nonempty dict , count a");
        AssertEqual(GetSynonymCount(synonyms, "b"), 1u, " Nonempty dict , count b");
        AssertEqual(GetSynonymCount(synonyms, "z"), 0u, " Nonempty dict , count z");
    }
    std::cout << " TestCount OK" << std::endl;
}

void TestAreSynonyms()
{ // тестируем AreSynonyms
    {
        Synonyms empty; // пустой словарь для любых двух слов вернёт false
        assert(!AreSynonyms(empty, "a", "b"));
        assert(!AreSynonyms(empty, "b", "a"));
    }
    {
        Synonyms synonyms = {
            {"a", {"b", "c"}},
            {"b", {"a"}},
            {"c", {"a"}}};
        assert(AreSynonyms(synonyms, "a", "b"));
        assert(AreSynonyms(synonyms, "b", "a"));
        assert(AreSynonyms(synonyms, "a", "c"));
        assert(AreSynonyms(synonyms, "c", "a"));
        assert(!AreSynonyms(synonyms, "b", "c"));
        assert(!AreSynonyms(synonyms, "c", "b"));
        // false
        // false
    }
    std::cout << " TestAreSynonyms OK" << std::endl;
}

void TestAll()
{ // функция, вызывающая все тесты
    TestCount();
    TestAreSynonyms();
    TestAddSynonyms();
}
int main()
{
    size_t fail_count = 0;
    try
    {
        TestAreSynonyms();
        // ловим исключение
    }
    catch (std::runtime_error &e)
    {
        ++fail_count;
        std::cout << " TestAreSynonyms "
             << " fail: " << e.what() << std::endl;
    } // если мы словили исключение, то работа всё равно продолжится
    try
    {
        TestCount();
    }
    catch (std::runtime_error &e)
    {
        ++fail_count;
        std::cout << " TestCount "
             << " fail: " << e.what() << std::endl;
    }
    try
    {
        TestAddSynonyms();
    }
    catch (std::runtime_error &e)
    {
        ++fail_count;
        std::cout << " TestAddSynonyms "
             << " fail: " << e.what() << std::endl;
    }
}